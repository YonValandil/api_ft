import ftapi from './ftapi/ftapi.js'

const getUser = async (client, id) => {
  return await client.get(`/v2/users/${id}`)
    .catch(err => console.error(err))
}

(async () => {
  const client = await ftapi()
    .catch(err => console.error(err))

  const id = '89532'

  const get = await getUser(client, id)
    .catch(err => console.error(err))
  console.log('get data => ', get.data)
  console.log('get status => ', get.status)
})().catch(err => console.error(err))
