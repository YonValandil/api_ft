import connection from './connection.js'
import { createUser as newUser } from './user/create.js'

export const createUser = newUser
export default connection
