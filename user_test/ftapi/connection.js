import axios from 'axios'
import config from './config.js'

const getToken = async (client, data) => {
  try {
    const token = await client.post('/oauth/token', data)
    client.defaults.headers.common.Authorization = 'Bearer ' + token.data.access_token
    return token
  } catch (error) {
    console.error(error)
  }
}

const interceptor = async (client, data) => {
  client.interceptors.response.use(
    success => success,
    async error => {
      const originalRequest = error.config
      if (
        error.response.status === 401 &&
          originalRequest.url.includes('/oauth/token')
      ) {
        return Promise.reject(error)
      } else if (error.response.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true
        const token = await getToken(client, data)
        originalRequest.headers.Authorization = 'Bearer ' + token.data.access_token
        return client(originalRequest)
      }
      return Promise.reject(error)
    }
  )
}

const connection = async (client_id = config.data.client_id, client_secret = config.data.client_secret) => {
  const client = axios.create({
    baseURL: config.baseurl
  })
  const data = {
    grant_type: config.data.grant_type,
    client_id,
    client_secret
  }

  await getToken(client, data)
  await interceptor(client, data)
  return client
}

export default connection
