import config from './config.js'

// A new user requires at least the email, campus_id, first_name and last_name fields.
// If the login isn’t specified, it will be generated from the supplied first_name and the last_name.
// If no cursus is supplied, the user will join the cursus piscine-c by default.

export const createUser = async (
  client,
  campus_id = config.user.campus_id,
  email = config.user.email,
  first_name = config.user.first_name,
  last_name = config.user.last_name,
  password = config.user.password
) => {
  const newConfig = { user: { ...config.user, campus_id, email, first_name, last_name, password } }

  return await client.post('/v2/users', newConfig).catch(err => console.error(err))
}
