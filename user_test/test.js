import ftapi from './ftapi/ftapi.js'

const updateUserConfig = {
  user:
  {
    // email: 'valandil.yon@gmail.com',
    // first_name: 'yon',
    // kind: 'student',
    // last_name: 'valandil',
    // password: '@YVtotoro1234@',
    // pool_month: 'september',
    // pool_year: '2019',
    // status: 'admis'
    cursus_users_attributes: [{
      id: 124870,
      begin_at: '2021-06-10 06:22:00 UTC',
      cursus_id: 9,
      end_at: '2070-03-01 13:43:10 UTC',
      has_coalition: true
    }]
  }
}

const campusConfig = {
  campus_user: {
    campus_id: '33',
    user_id: '89532',
    is_primary: true
  }
}

const cursusUserConfig = {
  cursus_user: {
    begin_at: '2021-06-10 12:45:00 UTC',
    cursus_id: '9',
    end_at: '2070-03-01 13:43:10 UTC',
    user_id: '89532'
  }
}

const updateUser = async (client, userId) => {
  return await client.put(`/v2/users/${userId}`, updateUserConfig)
    .catch(err => console.error(err))
}

const getUser = async (client, userId) => {
  return await client.get(`/v2/users/${userId}`)
    .catch(err => console.error(err))
}

const createCampus = async (client, userId) => {
  return await client.post(`/v2/users/${userId}/campus_users`, campusConfig)
    .catch(err => console.error(err))
}

const updateCursus = async (client, userId) => {
  return await client.put('/v2/cursus_users/1', {})
    .catch(err => console.error(err))
}

const createUserCursus = async (client, userId) => {
  return await client.post(`/v2/users/${userId}/cursus_users`, cursusUserConfig)
    .catch(err => console.error(err))
}

(async () => {
  const client = await ftapi()
    .catch(err => console.error(err))

  // const userId = '89532' //yvalandi
  const userId = '90121' // spanda

  // const listCampus = await client.get('/v2/campus').catch(err => console.log('err => ', err))
  // console.log('list campus => ', listCampus)

  const put = await updateUser(client, userId).catch(err => console.error(err))
  console.log('put status => ', put.status)

  // const campus = await createCampus(client, userId).catch(err => console.error(err))
  // console.log('campus => ', campus)
  // console.log('campus status => ', campus.status)

  // const cursus = await updateCursus(client, userId).catch(err => console.error(err))
  // console.log('cursus => ', cursus)
  // console.log('cursus status => ', cursus.status)

  // const newUserCursus = await createUserCursus(client, userId).catch(err => console.error(err))
  // console.log('newUserCursus => ', newUserCursus)
  // console.log('newUserCursus status => ', newUserCursus.status)

  const get = await getUser(client, userId).catch(err => console.error(err))
  console.log('get data => ', get.data)
  console.log('get status => ', get.status)
})().catch(err => console.error(err))
