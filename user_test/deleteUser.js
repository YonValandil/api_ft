import fns, { add } from 'date-fns'

// Test delete
// const del = await client.remove('/v2/users',
//   { email: 'valandil.yon@gmail.com' }
// ).catch(err => console.error(err))
// console.log('createUser req status => ', del.data.status)

const today = new Date()

// to return the date number(1-31) for the specified date
console.log("today => ",today)

let tomorrow = new Date()
tomorrow.setDate(new Date().getDate() + 1)

console.log('tomorrow => ', tomorrow)

const tomorrowFns = add(new Date(),{
  days: 1
})

console.log("tomorrow",tomorrowFns)
