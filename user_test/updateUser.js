import ftapi from './ftapi/ftapi.js'

const config = {
  user:
  {
    campus_id: '4',
    campus_name: 'Bangkok',
    // email: 'valandil.yon@gmail.com',
    first_name: 'yon',
    // kind: 'student',
    // last_name: 'valandil',
    // password: '@YVtotoro1234@',
    pool_month: 'september'
    // pool_year: '2017',
    // status: 'admis'
  }
}
const putUser = async (client, id) => {
  return await client.put(`/v2/users/${id}`, config)
    .catch(err => console.error(err))
}

(async () => {
  const client = await ftapi()
    .catch(err => console.error(err))

  const id = '89532'

  const res = await putUser(client, id)
    .catch(err => console.error(err))
  console.log('put status => ', res.status)
})().catch(err => console.error(err))
