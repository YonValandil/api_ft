import axios from 'axios'
import config from './config.js'

const getToken = async (client, data) => {
  try {
    const token = await client.post('/oauth/token', data)
    client.defaults.headers.common.Authorization = 'Bearer ' + token.data.access_token
    return token
  } catch (error) {
    console.error(error)
  }
}

const interceptor = async (client, data) => {
  client.interceptors.response.use(
    success => success,
    async error => {
      const originalRequest = error.config
      if (
        error.response.status === 401 &&
          originalRequest.url.includes('/oauth/token')
      ) {
        return Promise.reject(error)
      } else if (error.response.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true
        const token = await getToken(client, data)
        originalRequest.headers.Authorization = 'Bearer ' + token.data.access_token
        return client(originalRequest)
      }
      return Promise.reject(error)
    }
  )
}

const createClient = async (client_id = config.data.client_id, client_secret = config.data.client_secret) => {
  const client = axios.create({
    baseURL: config.baseurl
  })
  const data = {
    grant_type: config.data.grant_type,
    client_id,
    client_secret
  }

  await getToken(client, data)
  await interceptor(client, data)
  return client
}

(async () => {
  // const credentials = {
  //   client_id: ID_HERE,
  //   client_secret: SECRET_HERE
  // }
  // const client = await createClient(
  //   credentials.client_id,
  //   credentials.client_secret
  // )
  const client = await createClient() // Works without parameters (then use: config OR env)

  // Your code from here
  const tokeninfo = await client.get('/oauth/token/info').catch(err => console.error(err))

  const req = await client.get('/v2/cursus/42/users', {
    params: {
      page: 2,
      per_page: 50
    }
  }).catch(err => console.error(err))

  console.log(
    '\ntoken info => ', tokeninfo,
    '\ncall users, page: 2 / per: 50 => ', req.data.length,
    '\req link => ', req.headers.link,
    '\nx per page => ', req.headers['x-per-page'],
    '\nx page => ', req.headers['x-page'],
    '\napp role => ', req.headers['x-application-roles']
  )
})().catch(err => console.error(err))
