import dotenv from 'dotenv'

const env = dotenv.config().parsed

export default {
  baseurl: env.BASEURL,
  data: {
    grant_type: 'client_credentials',
    client_id: env.UID,
    client_secret: env.SECRET
  }
}
