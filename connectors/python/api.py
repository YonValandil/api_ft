# TODO : refresh token

import requests

BASEURL = 'https://api.intra.42.fr'
UID = 'b6107c8439b4a48b023c3385a19caf800e1e6d884000a683ffe45b6dc937d4e7'
SECRET = '266d6f24ca20858b28210c8a3ba0101c9b31fbe16bf7066c65f824ffd08eb822'

data = {'grant_type' : 'client_credentials', 'client_id' : UID, 'client_secret' : SECRET}

session = requests.Session()

token = session.post(BASEURL + '/oauth/token', data=data)
print(token.status_code)

json_response = token.json()
session.headers['Authorization'] = 'Bearer ' + json_response['access_token']

response = session.get(BASEURL + '/v2/cursus')

print(response.headers['content-type'])
print(response.content)