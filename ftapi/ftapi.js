import connection from './connection.js'
import { connectoauth as newoauth } from './connectoauth'
import { createUser as newUser } from './user/create.js'

export const createUser = newUser
export const connectoauth = newoauth

export default connection
